﻿"use strict";
/*
 
 CIT 3064
 Author: Elvia Del Toro
 Date: 02/22/2021

 */

/* Execute the function to run and display the countdown clock */
runClock();
setInterval("runClock()", 1000);

/* Function to create and run the countdown clock */

function runClock() {

/* Store the current day & time */
var currentDay = new Date();
var dateStr = currentDay.toLocaleDateString();
var timeStr = currentDay.toLocaleTimeString();



/* Display current date & time */
document.getElementById("todaysDate").innerHTML = dateStr +" "+ timeStr;


}