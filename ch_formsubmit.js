/*
 CIT3064
 Author: Elvia Del Toro
 Date: 02/23/2021
*/

window.onload = setForm;

function setForm() {
   document.forms[0].onsubmit = function() {
      if (this.checkValidity()) alert("No invalid data detected. Will retain data for further testing.");
      return false;
   }
}
